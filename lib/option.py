from lib.tile import Tile

class Option:
    posX = 0
    posY = 0
    display = False
    iconPath = ''
    name = ''

    def __init__(self, iconPath, posX, posY, display, menu):
        self.iconPath = iconPath
        self.setPosition(posX,posY)
        self.display = display
        self.icon = Tile((self.posX,self.posY), 'men', self.iconPath, None)
        print(self.iconPath + '-------------')
        self.menu = menu

    def setPosition(self, posX, posY):
        self.posX = posX
        self.posY = posY

    def draw(self, screen):
        self.icon.draw(screen)

    def execute(self):
        raise NotImplementedError()