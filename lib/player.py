import pygame

from lib.algorithm import algoA
from lib.building import Building


class Player:
    pseudo = ''
    listUnits = []
    listUnitsSelected = []
    menu = None

    def __init__(self, pseudo):
        self.pseudo = pseudo

    def linkMenu(self, menu):
        self.menu = menu
        hotel = Building((800, 300), 'men', './img/Towncentermiddle.png', 1, 1, self.menu)
        self.listUnits.append(hotel)

    def selectUnit(self, selectPositionStart, selectPositionEnd):
        selectedUnit = None
        for unit in self.listUnitsSelected:
            unit.isSelected = False
        self.listUnitsSelected = []
        for unit in self.listUnits:
            if selectPositionStart[0] <= unit.position[0] <= selectPositionEnd[0] and \
                    selectPositionStart[1] <= unit.position[1] <= selectPositionEnd[1]:
                selectedUnit = unit
                unit.isSelected = True
                self.listUnitsSelected.append(unit)
        return selectedUnit

    def moveSelectedUnits(self, listTile, goalTile):
        allPath = []
        for unit in self.listUnitsSelected:
            unit.currentTile = self.findUnitCurrentTile(unit, listTile)
            # print(unit.currentTile.position)
            if unit.currentTile != None:

                algo = algoA(listTile, unit.currentTile, goalTile, 25)

                path = algo.path[::-1]
                path.append(unit.currentTile.index)
                path = path[::-1]
                pathTile = self.findTilePath(path, listTile)
                unit.path = pathTile
                unit.tileToGo = pathTile[0]
                allPath.append(path)
        return allPath

    def drawUnits(self, screen, dx, dy):
        for unit in self.listUnits:
            unit.moveCamera(dx, dy)
            unit.draw(screen)

    def findUnitCurrentTile(self, unit, listTile):

        curretnTile = None
        for tile in listTile:
            if unit.position[0]+ unit.width/2 >= tile.position[0] and unit.position[0]+ unit.width/2 <= tile.position[0] + tile.width and unit.position[1]+ unit.height/2 >= tile.position[1] and unit.position[1]+ unit.height/2 <= tile.position[1] + tile.height:

                curretnTile = tile
        return curretnTile

    def findTilePath(self, path, listTile):
        pathTile =[]
        for index in path:
            pathTile.append(listTile[index])
        return pathTile