import pygame
class EventController:
    selectPositionStart = None
    selectPositionEnd = None
    lockSelection = False
    def __init__(self, game):
        self.game = game
        print('init')

    def click(self):
        print('click')

    def routine(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.running = False
            elif event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 3:
                    self.game.moveSelectedUnits()
                elif event.button == 1:
                    if self.lockSelection == False:
                        self.selectPositionStart = pygame.mouse.get_pos()
                        self.lockSelection = True
            elif event.type == pygame.MOUSEBUTTONUP:
                self.selectPositionEnd = pygame.mouse.get_pos()
                self.lockSelection = False
                self.game.select(self.selectPositionStart, self.selectPositionEnd)
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.game.running = False
                if event.key == pygame.K_t:
                    print('tileMode')
                    self.game.changeSelectMode('tile')
                if event.key == pygame.K_u:
                    self.game.changeSelectMode('unit')
                if event.key == pygame.K_m:
                    self.game.moveSelectedUnits()