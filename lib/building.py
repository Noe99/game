import pygame

from lib.options.creatVillager import CreatVillagerOption
from lib.unit import Unit


class Building(Unit):

    def draw(self, screen):
        if self.isSelected:
            self.updateMenu()
            pygame.draw.circle(screen, (0, 0, 255), (self.position[0] + self.width / 2, self.position[1] + self.height),
                               self.width / 2, width=1)
        self.source_area = pygame.Rect((0, 0),
                                       (self.width, self.height))
        screen.blit(self.tile, self.position, self.source_area)

    def updateMenu(self):
        self.menu.listOption = []
        creatVillagerOption = CreatVillagerOption('./img/icon/men.png', self.menu.position[0] + 25, self.menu.position[1] + 25, True, self.menu)
        self.menu.listOption.append(creatVillagerOption)
