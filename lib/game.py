import random
from lib.menu import Menu
from lib.player import Player
from lib.tile import Tile
from lib.algorithm import algoA
from PIL import Image
import pygame
pygame.init()
infoObject = pygame.display.Info()
screenWidth = infoObject.current_w
screenHeight = infoObject.current_h
screen = pygame.display.set_mode((screenWidth, screenHeight))


class Game:
    running = True
    mapSize = 25
    listTile = []
    listObject = []
    listEcosystemUnits = []
    tileWidth = 128
    tileHeight = 128
    way = 0
    startTile = None
    endTile = None
    currentTile = Tile((0,0), 'grass', './img/tile.png', None)
    selectMode = 'unit'
    player = Player('Noe')

    def __init__(self):
        j = 0
        while j < self.mapSize:
            i = 0
            while i < self.mapSize:
                p1 = (screenWidth / 2 + i * self.tileWidth / 2 - (self.tileWidth / 2) * j - (self.tileWidth / 2),
                        i * self.tileHeight / 4 + (self.tileHeight / 4) * j)
                tile = Tile(p1, 'grass', './img/tile1.png', i + j* self.mapSize)
                self.listTile.append(tile)
                self.listObject.append(tile)
                i += 1
            j += 1
        self.generateEcosystem('tree','./img/unit/object/Tree00{}.png', 100)
        self.generateEcosystem('gold', './img/unit/object/Gold00{}.png', 5)
        self.generateEcosystem('stone', './img/unit/object/stone00{}.png', 5)
        self.menu = Menu(screen, self.listTile, self.player)
        self.player.linkMenu(self.menu)


    def generateEcosystem(self,name, srcc, numberMax):
        index = 0
        nbObject = random.randint(0, numberMax)
        while index < nbObject:
            x = random.randint(0, self.mapSize-1)
            y = random.randint(0, self.mapSize-1)
            if self.listTile[y * self.mapSize + x].object == None:
                src = srcc.format(random.randint(1, 5))
                im = Image.open(src)
                imgWidth, imgHeight = im.size
                posTemp = self.listTile[y * self.mapSize + x].position
                posX = posTemp[0] + self.listTile[y * self.mapSize + x].width / 2 - imgWidth/2
                posY = posTemp[1] + self.listTile[y * self.mapSize + x].height / 4 - imgHeight
                pos = (posX,posY)
                object = Tile(pos, name, src, x + y * self.mapSize)
                self.listTile[y * self.mapSize + x].object = object
                self.listEcosystemUnits.append(object)
                self.listObject.append(object)
            index += 1

    def changeSelectMode(self, mode):
        self.selectMode = mode

    def select(self, selectPositionStart, selectPositionEnd):
        if (pygame.mouse.get_pos()[1] < self.menu.position[1]):
            if self.selectMode == 'tile':
                self.mapSelectTile()
            elif self.selectMode == 'unit':
                self.mapSelectUnit(selectPositionStart, selectPositionEnd)
        else:
            self.menuSelect()

    def menuSelect(self):

        self.menu.select()

    def mapSelectUnit(self, selectPositionStart, selectPositionEnd):

        unit = self.player.selectUnit(selectPositionStart, selectPositionEnd)

    def mapSelectTile(self):
        for object in self.listTile:
            if object.position == self.currentTile.position:
                if (self.way == 0):
                    object.tile = pygame.image.load('./img/start.png')
                    self.startTile = object
                elif (self.way == 1):
                    object.tile = pygame.image.load('./img/end.png')
                    self.endTile = object
                elif (self.way == 2):
                    algo = algoA(self.listTile, self.startTile, self.endTile, self.mapSize)
                    self.drawPath(algo.path)
                else:
                    self.way = -1
                    self.reloadMap()
                self.way += 1

    def reloadMap(self):
        for object in self.listTile:
            if object.type == 'grass':
                object.tile = pygame.image.load('./img/tile1.png')

    def drawPath(self,path):
        for tileIndex in path:
            self.listTile[tileIndex].tile = pygame.image.load('./img/start.png')

    def draw(self):
        for object in self.listObject:
            object.draw(screen)

    def mouveCamera(self,dx, dy):
        screen.fill((0, 0, 0))
        for object in self.listObject:
            object.move(dx, dy)
            object.draw(screen)
            if (object.position[0] < pygame.mouse.get_pos()[0] <= object.position[0] + object.width and
                    object.position[1] < pygame.mouse.get_pos()[1] <= object.position[1] + object.height / 4):
                if(object.type == 'grass'):
                    self.currentTile.position = object.position
                    self.currentTile.draw(screen)
        self.menu.moveUnits(dx,dy)

    def moveSelectedUnits(self):
        goalTile = None
        for object in self.listTile:
            if object.position == self.currentTile.position:
                object.tile = pygame.image.load('./img/end.png')
                goalTile = object
        self.player.moveSelectedUnits(self.listTile, goalTile)


    def move(self,):
        dx = 0
        dy = 0
        speed = 20
        if (pygame.mouse.get_pos()[0] < 0.1 * screenWidth and pygame.mouse.get_pos()[0] > 0 and pygame.mouse.get_pos()[1] < self.menu.position[1]):
            dx += speed
            dy += 0
        elif (pygame.mouse.get_pos()[0] > 0.9 * screenWidth and pygame.mouse.get_pos()[1] < self.menu.position[1]):
            dx += -speed
            dy += 0
        elif (pygame.mouse.get_pos()[1] < 0.1 * screenHeight and pygame.mouse.get_pos()[1] < self.menu.position[1]):
            dx += 0
            dy += speed
        elif (pygame.mouse.get_pos()[1] > 0.9 * (screenHeight-self.menu.height) and pygame.mouse.get_pos()[1] < (screenHeight-self.menu.height) ):
            dx += 0
            dy += -speed
        self.mouveCamera(dx, dy)
        self.menu.draw()
        self.player.drawUnits(screen, dx, dy)
