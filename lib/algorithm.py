import math

from lib.tile import Tile

class Node:
    parent = -1
    position = -1
    g = 0
    h = 0

    def __init__(self, position):
        self.position = position

class algoA:
    dim = 20
    openList = []
    closeList = []
    listNodes = []
    path = []
    def __init__(self, tileList, startTile, endTile, dim):
        self.reset()
        self.dim = dim
        self.tileList = tileList
        self.startTile = startTile
        self.endTile = endTile
        self.calcul()

    def reset(self):
        self.dim = 20
        self.openList = []
        self.closeList = []
        self.listNodes = []
        self.path = []

    def findIndex(self, tileToFind):
        i = 0
        while i < len(self.tileList):
            if self.tileList[i].position == tileToFind.position :
                return  i
            i+=1

    def findNeighbour(self, node):
        return node.position - 1, \
               node.position + 1, \
               node.position - self.dim, \
               node.position - self.dim - 1, \
               node.position - self.dim + 1, \
               node.position + self.dim, \
               node.position + self.dim - 1, \
               node.position + self.dim + 1

    def calcul(self):
        self.startIndex = self.findIndex(self.startTile)
        self.endIndex = self.findIndex(self.endTile)
        if self.startIndex != None :
            i=0
            for tile in self.tileList:
                node = Node(i)
                self.listNodes.append(node)
                if tile.object != None:
                    self.closeList.append(node)
                i+=1
            self.listNodes[self.startIndex].parent = self.startIndex
            self.openList.append(self.listNodes[self.startIndex])
            currentNodeIndex = 0
            z = 0
            #while z < 5:
            while self.openList[currentNodeIndex].position != self.endIndex:
                neighbours = self.findNeighbour(self.openList[currentNodeIndex])
                for neighbour in neighbours:
                    if(neighbour >= 0 and neighbour <= self.dim * self.dim - 1):
                        self.addNeighbour(neighbour, self.openList[currentNodeIndex])
                self.closeList.append(self.openList[currentNodeIndex])
                self.openList.pop(currentNodeIndex)
                currentNodeIndex = self.findCurrentNodeIndex()
                z += 1
            self.path = self.retraceParents(self.openList[currentNodeIndex])[::-1]


    def addNeighbour(self,neighbour, currentNode):
        alreadyExist = False
        for node in self.openList:
            if node.position == neighbour:
                alreadyExist = True
        for node in self.closeList:
            if node.position == neighbour:
                alreadyExist = True
        if alreadyExist == False:
            self.listNodes[neighbour].parent = currentNode.position
            self.listNodes[neighbour].g = self.calculG(currentNode, self.listNodes[neighbour])
            self.listNodes[neighbour].h = self.calculH(self.listNodes[self.endIndex], self.listNodes[neighbour])
            self.openList.append(self.listNodes[neighbour])




    def calculG(self, currentNode, node):
        if node.position == currentNode.position - self.dim or node.position ==currentNode.position + self.dim or node.position ==currentNode.position - 1 or node.position ==currentNode.position + 1:
            return currentNode.g + 10
        else:
            return currentNode.g + 14

    def calculH(self,endNode, node):
        dx = math.fabs(endNode.position%self.dim - node.position%self.dim)
        dy = math.fabs(int(endNode.position/self.dim) - int(node.position/self.dim))
        return (dx + int(dy))*10

    def findCurrentNodeIndex(self):
        minF = 10000000
        index = 0
        i =0
        for node in self.openList:
            f = node.g + node.h
            if f <= minF:
                minF = f
                index = i
            i+=1
        return index

    def retraceParents(self, currentNode):
        path = []
        while currentNode.position != self.startIndex:
            path.append(currentNode.position)
            currentNode = self.listNodes[currentNode.parent]
        return path
