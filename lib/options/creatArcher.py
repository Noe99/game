from lib.option import Option
from lib.unit import Unit

class CreatArcherOption(Option):
    name = 'villager'

    def execute(self):
        unitSrc = './img/unit/archerstand.png'
        unit = Unit((800, 300), 'men', unitSrc, 10, 1, self.menu)
        unit.setPosition(unit.position)
        self.menu.player.listUnits.append(unit)
        print('men have been created')