import pygame
from PIL import Image
class Tile:
    type = ''
    position = (0, 0)
    img = ''
    index = None
    object = None


    def __init__(self, position, type, img, index):
        if(type != ''):
            self.index = index
            self.type = type
            self.position = position
            self.img = img
            self.tile = pygame.image.load(self.img)
            im = Image.open(img)
            self.width, self.height = im.size
            self.source_area = pygame.Rect((0, 0), (self.width, self.height))

    def draw(self, screen):
        screen.blit(self.tile, self.position, self.source_area)

    def move(self, dx, dy):
        self.position = (self.position[0]+dx, self.position[1]+dy)