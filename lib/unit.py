import pygame
from PIL import Image


class Unit:
    type = ''
    position = (0, 0)
    img = ''
    currentDiv = 0
    isSelected = False
    currentTile = None
    path = []
    tileToGo = None
    currentAction = 0

    def __init__(self, position, type, img, div, divY, menu):
        self.menu = menu
        self.maximumDiv = div
        self.type = type
        self.position = position
        self.img = img
        self.tile = pygame.image.load(self.img)
        im = Image.open(img)
        self.width, self.height = im.size
        self.width = self.width / div
        self.height = self.height / divY
        self.source_area = pygame.Rect((0, 0), (self.width, self.height))

    def draw(self, screen):
        if self.tileToGo != None:
            self.goToTile(self.tileToGo)
        if self.isSelected:
            #self.updateMenu()
            pygame.draw.circle(screen, (0, 0, 255), (self.position[0] + self.width / 2, self.position[1] + self.height),
                               self.width / 2, width=1)
        # pygame.draw.rect(screen, (  255,  255, 255), (self.position[0], self.position[1], self.width, self.height))

        if self.type == 'men':
            self.source_area = pygame.Rect(((self.currentDiv) * self.width, self.currentAction * self.height),
                                           (self.width, self.height))

        screen.blit(self.tile, self.position, self.source_area)
        self.currentDiv += 1
        if self.currentDiv > self.maximumDiv - 1:
            self.currentDiv = 0

    def moveCamera(self, dx, dy):
        self.position = (self.position[0] + dx, self.position[1] + dy)

    def setPosition(self, position):
        self.position = position

    def goToTile(self, tile):
        dx = 0
        dy = 0
        speed = 4
        if self.position[0] < tile.position[0]:
            dx = speed
        if self.position[0] > tile.position[0]:
            dx = -speed
        if self.position[1] < tile.position[1]:
            dy = speed
        if self.position[1] > tile.position[1]:
            dy = -speed

        if self.position[0] >= tile.position[0] and self.position[0] <= tile.position[0] + tile.width and self.position[
            1] >= tile.position[1] and self.position[1] <= tile.position[1] + tile.height:
            if len(self.path) >= 1:
                self.path.pop(0)
                if len(self.path) == 0:
                    dx = 0
                    dy = 0
                    self.tileToGo = None
                    print('villager finish to move')
                else:
                    self.tileToGo = self.path[0]

        self.moveCamera(dx, dy)
        self.changeCurrentAction(dx, dy)

    def changeCurrentAction(self, dx, dy):
        if dx == 0 and dy == 0:
            # 'stand'
            self.currentAction = 0

        elif dx >= 1 and dy == 0:
            # moveRight'
            self.currentAction = 7
        elif dx <= -1 and dy == 0:
            # moveLeft'
            self.currentAction = 3

        elif dx == 0 and dy >= 1:
            # moveDown'
            self.currentAction = 1
        elif dx == 0 and dy <= -1:
            # mouveUp'
            self.currentAction = 5

        if dx >= 1 and dy >= 1:
            # mouveRightDown'
            self.currentAction = 6
        if dx <= -1 and dy <= -1:
            # 'mouveLeftUp'
            self.currentAction = 4
        if dx >= 1 and dy <= -1:
            # 'mouveRightUp'
            self.currentAction = 8
        if dx == -1 and dy >= 1:
            # 'mouveLeftDown'
            self.currentAction = 2

    def updateMenu(self):
        self.menu.genrateOption()
