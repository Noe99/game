import os

import pygame
from PIL import Image

from lib.algorithm import algoA
from lib.options.creatArcher import CreatArcherOption
from lib.tile import Tile
from lib.unit import Unit
from lib.options.creatVillager import CreatVillagerOption


class Menu:
    listUnits = []
    listUnitsSelected = []
    listOption = []
    def __init__(self, screen, listTile, player):
        self.player = player
        self.createMenuUnit = False
        self.listTile = listTile
        self.screen = screen
        self.width = screen.get_width()
        self.height = int(screen.get_height() * 0.25)
        self.position = (0, self.screen.get_height()-self.height)

        self.listBorderTile = []
        self.generateBorder()
        self.genrateOption()

    def reset(self):
        self.listOption = []

    def genrateOption(self):
        creatVillagerOption = CreatVillagerOption('./img/icon/men.png', self.position[0] + 25, self.position[1] + 25, True, self)
        self.listOption.append(creatVillagerOption)

        creatArcherOption = CreatArcherOption('./img/icon/archer.png', self.position[0] + 75, self.position[1] + 25, True, self)
        self.listOption.append(creatArcherOption)



    def generateBorder(self):
        hBorderSrc = './img/hBorder.png'
        vBorderSrc = './img/vBorder.png'
        im = Image.open(hBorderSrc)
        imgWidth, imgHeight = im.size
        i = 0
        while i < self.width / imgWidth:
            hBorderTile = Tile((i * imgWidth, 0), 'border', hBorderSrc, None)
            hBorderTile2 = Tile((i * imgWidth, self.position[1]), 'border', hBorderSrc, None)
            hBorderTile3 = Tile((i * imgWidth, self.screen.get_height() - imgHeight), 'border', hBorderSrc, None)
            self.listBorderTile.append(hBorderTile)
            self.listBorderTile.append(hBorderTile2)
            self.listBorderTile.append(hBorderTile3)
            i += 1

        im = Image.open(vBorderSrc)
        imgWidth, imgHeight = im.size
        i = 0
        while i < self.height / imgWidth:
            vBorderTile = Tile((0, i * imgHeight), 'border', vBorderSrc, None)
            vBorderTile2 = Tile((self.width - imgWidth / 1.2, i * imgHeight), 'border', vBorderSrc, None)
            self.listBorderTile.append(vBorderTile)
            self.listBorderTile.append(vBorderTile2)
            i += 1


    def draw(self):
        for unit in self.listUnits:
            unit.draw(self.screen)

        pygame.draw.rect(self.screen, (255, 238, 204), (0,self.screen.get_height()-self.height,self.width,self.height))

        for borderTile in self.listBorderTile:
            borderTile.draw(self.screen)

        for option in self.listOption:
            option.draw(self.screen)

        if len(self.listUnitsSelected) > 0:
            self.showUnitMenu(self.listUnitsSelected[0])

    def select(self):
        print('menu selected icon = ')
        selectedOption = None
        for option in self.listOption:
            if pygame.mouse.get_pos()[0] >= option.icon.position[0] and pygame.mouse.get_pos()[0] <= option.icon.position[0] + option.icon.width and pygame.mouse.get_pos()[1] >= option.icon.position[1] and pygame.mouse.get_pos()[1] <= option.icon.position[1] + option.icon.height:
                selectedOption = option
            else:
                print('nothing here')
        if selectedOption != None :
            selectedOption.execute()


    def moveUnits(self, dx, dy):
        for unit in self.listUnits:
            unit.moveCamera(dx,dy)


    def selectUnit(self, selectPositionStart, selectPositionEnd):
        selectedUnit = None
        for unit in self.listUnitsSelected:
            unit.isSelected = False
        self.listUnitsSelected = []
        for unit in self.listUnits:
            if unit.position[0] >= selectPositionStart[0] and unit.position[0] <= selectPositionEnd[0] and unit.position[1] >= selectPositionStart[1] and unit.position[1] <= selectPositionEnd[1]:
                selectedUnit = unit
                unit.isSelected = True
                self.listUnitsSelected.append(unit)
        return selectedUnit



    def showUnitMenu(self, unit):
        if unit.type == 'men' and self.createMenuUnit == False:
            pos = (self.position[0] + self.width/2, self.position[1] + 25)
            icon = Tile(pos, 'house', './img/unit/houseIcon.png', None)
            self.listIcon.append(icon)
            self.createMenuUnit = True
