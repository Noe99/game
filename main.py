import pygame
from lib.game import Game
from lib.game import screenWidth
from lib.game import screenHeight
from lib.game import screen
from lib.eventController import EventController

game = Game()
game.draw()
eventController = EventController(game)
while eventController.game.running:
    position = (screenWidth/2 , screenHeight/2)
    game.move()
    eventController.routine()
    if eventController.lockSelection:
        pygame.draw.rect(screen, (255, 255, 255), (eventController.selectPositionStart[0],
                                                   eventController.selectPositionStart[1],
                                                   pygame.mouse.get_pos()[0] - eventController.selectPositionStart[0],
                                                   pygame.mouse.get_pos()[1] - eventController.selectPositionStart[1]), width=1)

    pygame.display.update()


